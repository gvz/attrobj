trait TypeEq<Other: ?Sized> {
    fn teq() -> bool {
        false
    }
}

impl<A: ?Sized> TypeEq<A> for A {
    fn teq() -> bool {
        true
    }
}
macro_rules! assert_teq {
    ($a:ty, $b:ty) => {
        assert!(<$a as TypeEq<$b>>::teq())
    };
}

pub trait Show {
    fn show<K, R>(k: K) -> R
    where
        K: Fn(&dyn std::fmt::Display) -> R;

    fn err() {
        Self::show(|a| eprintln!("{}", a))
    }

    fn string() -> String {
        Self::show(|a| format!("{}", a))
    }
}
macro_rules! show {
    ($t:path, $disp:expr) => {
        show!(; $t, $disp);
    };
    ( $( < $($vars:ident),* > )? ; $t:ty, $disp:expr) => {
        impl $( < $($vars: ?Sized),* > )? Show for $t
        where
            $( $( $vars: Show ),* )?
        {
            fn show<K, R>(k: K) -> R
            where
                K: Fn(&dyn std::fmt::Display) -> R,
            {
                let k: &dyn Fn(&dyn std::fmt::Display) -> R = &k;
                let block: &dyn Fn(&dyn Fn(&dyn std::fmt::Display) -> R) -> R = &$disp;
                block(k)
            }
        }
    };
    ( @expr $name:ty, $expr:expr ) => {
        show! { ; $name, |k: &dyn Fn(&dyn std::fmt::Display) -> R| k(&$expr) }
    }
}

mod runtime {
    use super::*;
    mod data {
        use super::*;
        use ::list::runtime::Data;

        macro_rules! assert_trivial_data {
            ($name:ident) => {
                #[test]
                fn $name() {
                    assert_teq!($name, Data<$name>);
                }
            };
        }

        #[allow(non_camel_case_types)]
        type unit = ();
        assert_trivial_data!(unit);
        assert_trivial_data!(bool);
        assert_trivial_data!(u32);
        assert_trivial_data!(str);
    }
}

mod typelevel {
    use ::list::core::*;
    type L = list::List![String, bool, (), u32];

    #[test]
    fn list_expressions() {
        use super::TypeEq;
        use expr::{Head, Tail};
        use list::core::*;
        assert_teq!(Head<L>, String);
        assert_teq!(Head<Tail<L>>, bool);
        assert_teq!(Head<Tail<Tail<L>>>, ());
        assert_teq!(Head<Tail<Tail<Tail<L>>>>, u32);
        assert_teq!(Tail<Tail<Tail<Tail<L>>>>, data::Unit);
        assert_teq!(Tail<Tail<Tail<Tail<Tail<L>>>>>, data::Unit);
        assert_teq!(Head<Tail<Tail<Tail<Tail<Tail<L>>>>>>, ());
    }

    #[test]
    fn lib_stuff() {
        use super::TypeEq;
        use list::{
            core::{
                lib::{CZong, Const, False, Id, Lazy, Lazy2, True, Zong},
                *,
            },
            Eval,
        };
        assert_teq!(Eval![Zong, L], CZong);
        assert_teq!(Eval![Id, L], L);
        assert_teq!(Eval![Const<()>, L], ());
        assert_teq!(Eval![Const<Const<()>>, L], Const<()>);
        assert_teq!(Eval![Lazy<True, Const<u8>>, L], u8);
        assert_teq!(Eval![Lazy<False, Id>, L], CZong);

        assert_teq!(Eval![Lazy2<False, Zong, Const<True>>, L], Zong);
        assert_teq!(Eval![Lazy2<True, Zong, Id>, L], L);
    }

    #[test]
    fn explosion() {
        use super::Show;
        use list::{
            core::{
                data::{List, Unit},
                lib::{Const, Id, Zong},
                *,
            },
            Def, Eval, List,
        };
        use std::marker::PhantomData as __;

        use lib::{False, If, True};
        Def! { @v2 IsTrivial
            @ u32 => True
            @ Attribute<A> => False = A
            @ u8 => False
        }

        pub struct Attribute<A: ?Sized>(__<A>);
        pub trait IAttribute {
            type RuntimeTime: ?Sized;
        }
        pub trait Spinach {}
        impl IAttribute for dyn Spinach {
            type RuntimeTime = String;
        }

        Def! { @v2 IsAttribute
            @ Attribute<A> => True = A
            @ u32 => False
            @ u8 => False
        }

        Def! { @v2 Runtime
            @ Attribute<A> => A::RuntimeTime
            > A where
                A: IAttribute
        }

        struct IsTrivial2;
        type RtExpr = If<IsTrivial, Id, If<IsAttribute, Runtime, Zong>>;
        // type Cond = lib::Const<T>;

        show!(@expr u8, "u8");
        show!(@expr u32, "u32");
        show!(@expr String, "String");
        // show!(@expr (), "()");
        show!(@expr True, "True");
        show!(@expr False, "False");
        show!(@expr IsTrivial, "IsTrivial");
        show!(@expr data::Unit, "Nil");
        show! { <A, B>; dyn List<A, B>, |k| A::show(|a| B::show(|b| k(&format_args!("{}, {}", a, b)))) }
        show!(@expr lib::CZong, "CZong");

        <Eval![RtExpr, u32]>::err();
        <Eval![RtExpr, Attribute<dyn Spinach>]>::err();
        lib::Eval::<RtExpr, u8>::err();

        use lib::Lazy2;
        type Superman = Lazy2<False, lib::True, lib::Const<False>>;
        <Eval![Superman, lib::Zong]>::err();
        <List![True, False]>::err();

        assert!(false);
    }
}
