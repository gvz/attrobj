use crate as list;

macro_rules! trivial_runtime_data {
    ($t:ty) => {
        impl list::runtime::IData for $t {
            type Data = $t;
        }
    };
}

trivial_runtime_data! { u32 }
trivial_runtime_data! { () }
trivial_runtime_data! { bool }
trivial_runtime_data! { str }

impl<'a, T> list::runtime::IData for &'a T {
    type Data = &'a T;
}
