pub mod core {
    pub mod data {
        use super::lang;

        pub trait List<Head: ?Sized, Tail: ?Sized> {}
        pub type Unit = dyn List<(), ()>;
        impl<Head, Tail> lang::IList for dyn List<Head, Tail>
        where
            Head: ?Sized,
            Tail: lang::IList + ?Sized,
        {
            type Head = Head;
            type Tail = Tail;
        }
        impl lang::IList for Unit {
            type Head = ();
            type Tail = Self;
        }
    }

    pub mod lang2 {
        macro_rules! Lang {
            (
                domain: $domain:ident,
            ) => {
                pub trait $domain {}
                pub trait Eval<Expr: $domain + ?Sized> {
                    type Eval;
                }
            };
        }

        Lang! {
            domain: Bogus,
        }
    }

    pub mod lang {
        pub struct True;
        pub struct False;

        pub trait IList {
            type Head: ?Sized;
            type Tail: IList + ?Sized;
        }

        pub trait Eval<Expr: ?Sized> {
            type Output: ?Sized;
        }

        pub trait Bool: IfElse<True, False> {}
        impl<S: IfElse<True, False>> Bool for S {}

        pub trait IfElse<T: ?Sized, F: ?Sized> {
            type IfElse: ?Sized;
            type Not: ?Sized + IfElse<T, F>;
        }
        impl<T: ?Sized, F: ?Sized> IfElse<T, F> for True {
            type IfElse = T;
            type Not = False;
        }
        impl<T: ?Sized, F: ?Sized> IfElse<T, F> for False {
            type IfElse = F;
            type Not = True;
        }
    }
    pub mod expr {
        use super::*;
        pub type Head<L> = <L as lang::IList>::Head;
        pub type Tail<L> = <L as lang::IList>::Tail;
        pub type Second<L> = Head<Tail<L>>;
    }
    pub mod lib {
        use super::*;
        #[macro_use]
        mod macros;
        use crate as list;

        pub use lang::False;
        pub use lang::True;
        pub use std::marker::PhantomData as __;

        pub struct CZong;

        pub type Eval<Expr, Slf> = <Slf as lang::Eval<Expr>>::Output;

        // Consistently returns [`CZong`]
        Def! { @v2
            Zong
            @ S => CZong = S
        }

        // Returns stream item unchanged
        Def! { @v2
            Id
            @ S => S = S
        }

        // Consistently returns `Val`, unevaluated
        Def! { @v2
            <Val> Const
            <Val> @ S => Val = S.Val
        }

        // Lazily evaluates `Expr` on condition
        Def! { @v2
            <Flip, IfOff, Expr> Lazy2

            <True, __, Expr> @ S => Eval![Expr, S]
            = S.Expr.__ where
                S: lang::Eval<Expr>

            <False, Off, __> @ S => Off
            = S.Off.__
        }

        Def! { @v2
            <Flip, Expr> Lazy

            <Flip, Expr> @ S => _R
            > S.Flip.Expr._R where
                S: lang::Eval<Lazy2<Flip, lib::CZong, Expr>, Output = _R>
        }

        Def! { @v2
            <Expr> Not

            <E> @ S => Val::Not
            > S.E.Val where
                S: lang::Eval<E, Output = Val>,
                Val: lang::IfElse<lang::False, lang::True>
        }

        Def! { @v2
            <Cond, T, F> If

            <C, T, F> @ S => _C::IfElse
            > S.C._C._NC.T._T.F._F
            where
                S: lang::Eval<C, Output = _C>,
                S: lang::Eval<Not<C>, Output = _NC>,
                S: lang::Eval<Lazy<_C, T>, Output = _T>,
                S: lang::Eval<Lazy<_NC, F>, Output = _F>,
                _C: lang::IfElse<_T, _F>
        }

        // == v2 ==

        // pub struct Term;
        // Def! { @v2
        //     <After, Before, Interpretation> Extend

        //     <Term, Expr, List![True]> @ S => lib::Eval<Expr, S>
        //     = S.Expr where
        //         S: lang::Eval<Expr>

        //     <D, Extend<B, BB>, data::List<True, I>> @ S => lib::Eval<D, S>
        //     = S.B.BB.I where
        //         S: lang::Eval<D>,

        // }
    }
}

pub mod runtime {
    pub trait IData {
        type Data: ?Sized;
    }

    pub type Data<T> = <T as IData>::Data;

    mod std;
}
