#[macro_export]
macro_rules! List {
    () => {
        data::Unit
    };

    ( $(,)? $head:ty $(, $tail:ty)* ) => {
        dyn data::List <$head, list::List!( $(, $tail)* )>
    }
}

#[macro_export]
macro_rules! Eval {
    ($expr:ty, $state:ty) => {
        lib::Eval<$expr, $state>
    };
}

#[macro_export]
macro_rules! Def {
    (
        @v2
        $( // ? <>
            <
            $( // * ,Var:Trait
                $(,)?
                $var_name: ident
                $( // ? :Trait
                    :
                    $var_type: ty
                )? // :Trait
            )* // ,Var:Trait
            >
        )? //<>

        $name: ident

        $( // + slf
            $( < $( $expr:path ),* > )?
            @ $slf:ty
            => $output:ty
            $( > $( $impl_var_name:ident ).* )?
            $(
                where
                $(
                    $where_expr:ty : $where_constraint:path
                ),*
            )?
        )+
    ) => {
        // pub const $name: &'static str = stringify! {
        Def! {
            $( < $( $var_name ),* > )?
            $name
            $(
                $( . $( $impl_var_name ).* )?
                $( < $($expr),* > )?
                @ $slf
                => $output
                $( where $( $where_expr : $where_constraint ),* )?
            )+
        }
        // };
    };
    (
        @v2
        $( // ? <>
            <
            $( // * ,Var:Trait
                $(,)?
                $var_name: ident
                $( // ? :Trait
                    :
                    $var_type: ty
                )? // :Trait
            )* // ,Var:Trait
            >
        )? //<>

        $name: ident

        $( // + slf
            $( < $( $expr:path ),* > )?
            @ $slf:ty
            => $output:ty
            $( = $( $impl_var_name:ident ).* )?
            $(
                where
                $(
                    $where_expr:ty : $where_constraint:path
                ),*
            )?
        )+
    ) => {
        // pub const $name: &'static str = stringify! {
        Def! {
            $( < $( $var_name ),* > )?
            $name
            $(
                $( . $( $impl_var_name ).* )?
                $( < $($expr),* > )?
                @ $slf
                => $output
                $( where $( $where_expr : $where_constraint ),* )?
            )+
        }
        // };
    };
    ( @decl
        $( // ? <>
            <
            $( // * ,Var:Trait
                $(,)?
                $var_name: ident
                $( // ? :Trait
                    :
                    $var_type: ty
                )? // :Trait
            )* // ,Var:Trait
            >
        )? //<>

        $name: ident
    ) => {
        pub struct $name$(< $( $var_name: ?Sized , )* >)?(
            lib::__<list::List![ $( $(, $var_name)* )? ]>
        );
    };
    (
        $( // ? <>
            <
            $( // * ,Var:Trait
                $(,)?
                $var_name: ident
                $( // ? :Trait
                    :
                    $var_type: ty
                )? // :Trait
            )* // ,Var:Trait
            >
        )? //<>

        $name: ident

        $( // + slf
            $( . $( $impl_var_name:ident ).* )?
            $( < $( $expr:path ),* > )?
            @ $slf:ty
            => $output:ty
            $(
                where
                $(
                    $where_expr:ty : $where_constraint:path
                ),*
            )?
        )+
    ) => {
        pub struct $name$(< $( $var_name: ?Sized , )* >)?(
            lib::__<list::List![ $( $(, $var_name)* )? ]>
        );

        // impl <
        //     U: ?Sized,
        //     Slf: ?Sized,
        //     $( $( $var_name: ?Sized ),* )?
        // >
        // IEvalable<
        //     lib::I<
        //         $name $( < $( $var_name ),* > )?,
        //         True,
        //         U
        //     >,
        //     $name $( < $( $var_name ),* > )?
        // >
        // for Slf
        // where
        //     Slf: lang::Eval<$name $(< $( $var_name ),* > )? >
        // {
        // }

        Def! {
            @call_tuple
            $name;
            < $(//+slf
                $slf => $output ;
                $( < $($expr),* > )? ;
                $( $($impl_var_name),* )? ;
                $( where $( $where_expr : $where_constraint ),* )? ;
            ),+ >//slf
            // tuple:tt
            { $( < $( $var_name ),* > )? }
        }
    };

    ( @call_tuple
        $name:ident;
        < $(//+slf
            $slf:ty => $output:ty;
            $( < $( $expr:path ),* > )? ;
            $( $impl_var_name:ident ),* ;
            $( where $( $where_expr:ty : $where_constraint:path ),* )? ;
        ),+ >//slf
        $tuple:tt
    ) => {
        $(//+slf
            Def! {
                @call
                $name;
                $slf => $output;
                $( < $( $expr ),* > )? ;
                $( where $( $where_expr : $where_constraint ),* )? ;
                $tuple
                { < $( $impl_var_name ),* > }
            }
        )+//slf
    };

    ( @ext $name:ident : $base:ty;
        $( // + slf
            $( < $( $expr:path ),* > )?
            $( . $( $impl_var_name:ident ).* )?
            @ $slf:ty
            => $output:ty
            $(
                where
                $(
                    $where_expr:ty : $where_constraint:path
                ),*
            )?
        )+
    ) => {
        impl
        lang::Eval<
            Extend<$name, $base>
        >
        for List![List![True, _], $slf]
        where
            $slf: lang::Eval<$name>
        {
            type Output = Eval![$name, Self];
        }
    };

    ( @call
        $name:ident;
        $slf:ty => $output:ty ;
        $( < $( $expr:path),* > )? ;
        $( where $( $where_expr:ty : $where_constraint:path ),* )? ;
        { $(< $($var_name:ident),* >)? }
        { $(< $($impl_var_name:ident),* >)? }
    ) => {

        // impl <
        //     $( $( $impl_var_name: ?Sized ),* )?
        // >
        // lang::Sig<
        //     $name $( < $( $expr ),* > )?,
        //     $slf
        // >
        // for list::typelevel::lib::True
        // $(
        //     where
        //     $(
        //         $where_expr : $where_constraint
        //     ),*
        // )?
        // {
        //     type Slf = Self;
        // }

        impl <
            $( $( $impl_var_name: ?Sized ),* )?
        >
        lang::Eval<
            // list::typelevel::lib::True,
            $name $( < $( $expr ),* > )?
        >
        for $slf
        $(
        where
            $(
                $where_expr : $where_constraint
            ),*
        )? {
            type Output = $output;
        }
    };
}
